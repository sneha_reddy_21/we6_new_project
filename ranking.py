with open("studentRanking_Asokan.txt") as file:
    for line in file:
        line = line.strip()

        name, list_marks = line.split(" ", 1)
         marks = list((int, list_marks.replace(",", " ").split()))


def compare_students(student1, student2):
    marks1 = student1[1]
    marks2 = student2[1]

    greater_count1 = sum(1 for m1, m2 in zip(marks1, marks2) if m1 > m2)
    greater_count2 = sum(1 for m1, m2 in zip(marks2, marks1) if m2 > m1)

    if greater_count1 == len(marks1):
        return -1
    elif greater_count2 == len(marks1):
        return 1
    else:
        return 0 
        
 





